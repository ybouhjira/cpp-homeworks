TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    matrice.cpp \
    vecteur.cpp

include(deployment.pri)
qtcAddDeployment()

HEADERS += \
    matrice.h \
    vecteur.h

