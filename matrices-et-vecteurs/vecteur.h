#ifndef VECTEUR3_H
#define VECTEUR3_H

class Vecteur
{
public:
    Vecteur(float x = 0, float y = 0, float z = 0);

    Vecteur operator+ (const Vecteur &other) const;

    Vecteur operator- (const Vecteur &other) const;

    float operator* (const Vecteur &other) const;

    float& operator[](int index);

    float operator[](int index) const;

    void print() const;

private:
    float m_data[3];
};

#endif // VECTEUR3_H
