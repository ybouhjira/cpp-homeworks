#include "matrice.h"
#include <cassert>
#include <iostream>

using std::endl;
using std::cout;

Matrice::Matrice(int rows, int columns)
{
    m_rows = rows;
    m_columns = columns;

    m_data = new float*[rows] ;
    for (int i = 0; i < rows; i++) {
        m_data[i] = new float[columns];
    }
}

Matrice::Matrice(const Matrice &other)
{
    m_columns = other.m_columns;
    m_rows = other.m_rows;

    m_data = new float*[m_rows] ;
    for (int i = 0; i < m_rows; i++) {
        m_data[i] = new float[m_columns];

        for (int j = 0; j < m_columns; ++j) {
            m_data[i][j] = other.m_data[i][j];
        }
    }
}

Matrice& Matrice::operator= (const Matrice &other)
{
    if (this != &other)
    {
        m_columns = other.m_columns;
        m_rows = other.m_rows;

        this->~Matrice();

        m_data = new float*[m_rows] ;
        for (int i = 0; i < m_rows; i++) {
            m_data[i] = new float[m_columns];

            for (int j = 0; j < m_columns; ++j) {
                m_data[i][j] = other.m_data[i][j];
            }
        }
    }

    return *this;
}

Matrice::~Matrice()
{
    if (m_data) {
        for(int i = 0; i < m_rows; i++) {
            if (m_data[i]) {
                delete[] m_data[i];
                m_data[i] = 0;
            }
        }

        delete[] m_data;
        m_data = 0;
    }
}

Matrice Matrice::sum (const Matrice &other, int sign) const
{
    assert(sign == 1 || sign == -1);

    if (other.m_rows != m_rows || other.m_columns != m_columns) {
        cout << "Erreur les matrices doivent être de même taille" << endl;
        return Matrice();
    }

    Matrice result(m_rows, m_columns);

    for (int i = 0; i < m_rows; i++) {
        for (int j = 0; j < m_columns; ++j) {
            result.m_data[i][j] = m_data[i][j] + sign * other.m_data[i][j];
        }
    }

    return result;
}

Matrice Matrice::operator+ (const Matrice &other) const
{
    return sum(other, 1);
}

Matrice Matrice::operator- (const Matrice &other) const
{
    return sum(other, -1);
}

float Matrice::operator* (const Vecteur &vector) const
{
    if (m_columns != 3) {
        cout << "Erreur, le nombre de colonnes doit être 3" << endl;
        return 0;
    }

    float result = 0;

    for (int i = 0; i < m_rows; i++) {
        for (int j = 0; j < m_columns; ++j) {
            result += m_data[i][j] * vector[j];
        }
    }

    return result;
}

float& Matrice::operator() (int row, int column)
{
    assert(row >= 0 && row < m_rows);
    assert(column >= 0 && column < m_columns);

    return m_data[row][column];
}

float Matrice::operator() (int row, int column) const
{
    assert(row >= 0 && row < m_rows);
    assert(column >= 0 && column < m_columns);

    return m_data[row][column];
}

void Matrice::print () const
{
    for (int i = 0; i < m_rows; ++i) {
        cout << "|";
        for (int j = 0; j < m_columns; ++j) {
            cout << m_data[i][j];
            if (j != m_columns - 1) {
                cout << ", ";
            }
        }
        cout << "|" << endl;
    }
}
