#ifndef MATRICE_H
#define MATRICE_H

#include "vecteur.h"

class Matrice
{
public:
    Matrice(int rows = 2, int columns = 2);

    ~Matrice();

    Matrice (const Matrice &other);

    Matrice& operator= (const Matrice &other);

    Matrice operator+ (const Matrice &other) const;

    Matrice operator- (const Matrice &other) const;

    float operator* (const Vecteur &vector) const;

    float& operator()(int row, int column);

    float operator()(int row, int column) const;

    void print () const;

private:
    Matrice sum (const Matrice &other, int sign) const;

private:
    int m_rows;
    int m_columns;
    float **m_data;
};

#endif // MATRICE_H
