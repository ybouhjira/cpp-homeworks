#include "vecteur.h"
#include <iostream>

using std::cout;
using std::endl;

Vecteur::Vecteur(float x, float y, float z)
{
    m_data[0] = x;
    m_data[1] = y;
    m_data[2] = z;
}

Vecteur Vecteur::operator+ (const Vecteur &other) const
{
    Vecteur result(m_data[0] + other.m_data[0]
            ,m_data[1] + other.m_data[1]
            ,m_data[2] + other.m_data[2]);

    return result;
}

Vecteur Vecteur::operator- (const Vecteur &other) const
{
    Vecteur result(m_data[0] - other.m_data[0]
            ,m_data[1] - other.m_data[1]
            ,m_data[2] - other.m_data[2]);

    return result;
}

float Vecteur::operator* (const Vecteur &other) const
{
    float result = 0;

    for (int i = 0; i < 3; i++) {
        result += m_data[i] * other.m_data[i];
    }

    return result;
}

void Vecteur::print () const
{
    cout << "(" << m_data[0] << ", "
         << m_data[1] << ", "
         << m_data[2] << ")" << endl;
}

float& Vecteur::operator[](int index)
{
    return m_data[index];
}

float Vecteur::operator[](int index) const
{
    return m_data[index];
}
