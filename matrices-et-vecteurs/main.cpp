#include <iostream>
#include "matrice.h"
#include <exception>

using namespace std;

int main()
{
    Matrice m(2, 3);

    for (int i = 0; i < 2; i++) {
        for (int j = 0; j < 3; j++) {
            m(i, j) = 1;
        }
    }

    Matrice n(m);
    Matrice sum = m + n;
    Matrice sub = m - n;

    sum.print();
    cout << endl;
    sub.print();

    sub = sum;

    sub.print();

    Vecteur v1(1, 2, 3);
    Vecteur v2(v1);

    (v1 + v2).print();
    (v1 - v2).print();

    return 0;
}

