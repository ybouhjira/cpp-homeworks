#ifndef SHAREDSTRING_H
#define SHAREDSTRING_H

#include <iostream>

class SharedString
{
public:
    SharedString (const char *string);

    SharedString (const SharedString &string);

    ~SharedString ();

    operator const char* () const;

    SharedString& operator= (const SharedString &other);

    char& operator[] (int index);

    char operator[] (int index) const;

private:
    char *m_string;

    unsigned int *m_ownersCount;
};


#endif // SHAREDSTRING_H
