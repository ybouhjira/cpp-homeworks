#ifndef CITY_H
#define CITY_H

#include <iostream>
#include "sharedstring.h"

class City
{
public:
    City (const SharedString &toponyme, int population);

    void info() const;

private:
    SharedString m_toponyme;
    int m_population;
};

#endif // CITY_H
