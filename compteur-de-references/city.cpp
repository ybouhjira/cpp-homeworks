#include "city.h"
#include <cstring>
#include <iostream>

using std::cout;
using std::endl;

City::City (const SharedString &toponyme, int population)
    : m_toponyme(toponyme)
    , m_population(population)
{

}

void City::info() const
{
    cout << "Ville : " << m_toponyme
         << ", population : " << m_population << endl;
}
