TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    city.cpp \
    main.cpp \
    sharedstring.cpp

HEADERS += \
    city.h \
    sharedstring.h
