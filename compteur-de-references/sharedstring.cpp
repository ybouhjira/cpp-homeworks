#include "sharedstring.h"
#include <cstring>

SharedString::SharedString (const char *string)
    : m_string(new char[strlen(string) + 1])
    , m_ownersCount(new unsigned int(1))
{
    strcpy(m_string, string);
}

SharedString::SharedString(const SharedString &string)
    : m_string(string.m_string)
    , m_ownersCount(string.m_ownersCount)

{
    (*m_ownersCount)++;
}

SharedString::~SharedString()
{
    if ((*m_ownersCount) == 1) {
        delete m_ownersCount;
        delete[] m_string;
    } else {
        (*m_ownersCount)--;
    }
}

SharedString::operator const char* () const
{
    return m_string;
}

SharedString& SharedString::operator=(const SharedString &other)
{
    if (this != &other) {
        if ((*m_ownersCount) == 1) {
            delete m_ownersCount;
            delete[] m_string;
        } else {
            (*m_ownersCount)--;
        }

        m_ownersCount = other.m_ownersCount;
        m_string = other.m_string;
        (*m_ownersCount)++;
    }

    return *this;
}

char& SharedString::operator [](int index)
{
    return m_string[index];
}

char SharedString::operator [](int index) const
{
    return m_string[index];
}
