#include <iostream>
#include "city.h"
#include "sharedstring.h"

using namespace std;

int main(void)
{
    City mohammedia("Mohamedia", 2);
    City moh2 = mohammedia;

    mohammedia.info();
    moh2.info();

    City c3("test", 4);
    c3.info();
    c3 = moh2;

    c3.info();

    return 0;
}

