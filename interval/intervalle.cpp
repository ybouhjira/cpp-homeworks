#include "intervalle.h"

Interval::Interval(double binf, double bsup)
{
    assert(binf <= bsup);

    _binf = binf;
    _bsup = bsup;
}

Interval Interval::operator+ (const Interval &other) const
{
    return Interval(_binf + other._binf, _bsup + other._bsup);
}

Interval Interval::operator- (const Interval &other) const
{
    return Interval(_binf - other._binf, _bsup - other._bsup);
}
