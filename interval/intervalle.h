#ifndef INTERVALLE_H
#define INTERVALLE_H

/**
  * TYPE interval, use TYPE Real, use Bool
  * OPERATIONS
  * Union : interval x interval --> interval
  * Intersection : interval x interval --> Bool
  * Contient : Real x interval --> Bool
  * Inclusion : interval < interval --> Bool
  *
  * AXIOMES
  * interval(a, b) --> a <= b
  *
  * PRE-COND
  * interval(a, b) U interval(c, d) si b <= c
  *
  */

class Interval
{
public:
    Interval(double binf=0, double binf=0);

    Interval operator+ (const Interval &other) const;

    Interval operator- (const Interval &other) const;

private:
    double _binf;
    double _bsup;
};

#endif // INTERVALLE_H
