TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    intervalle.cpp

include(deployment.pri)
qtcAddDeployment()

HEADERS += \
    intervalle.h

