#ifndef ILIS_STRING_H
#define ILIS_STRING_H


namespace ILISI {

    class String
    {
    public:
        String (const char *str);

        ~String();

        int longueur() const;

        String (const String &other);

        String& operator= (const String &other);

        char& operator[] (int index);

        char operator[] (int index) const;

        String operator+ (const String &other) const;

        bool operator== (const String &other) const;

        bool operator!= (const String &other) const;

    private:
        char *_string;
    };

} // namespace ilis

#endif // ILIS_STRING_H
