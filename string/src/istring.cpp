#include "istring.h"

#include <cstring>
#include <cassert>

using namespace ILISI;

String::String(const char *str)
{
    _string = new char[std::strlen(str) + 1];
    strcpy(_string, str);
}

String::String(const String &other)
{
    _string = new char[strlen(other._string) + 1];
    strcpy(_string, other._string);
}

String::~String()
{
    if(_string) {
        delete[] _string;
        _string = 0;
    }
}

int String::longueur() const
{
    return strlen(_string);
}

String& String::operator= (const String &other)
{
    if (this != &other) {
        this->~String();
        _string = new char[strlen(other._string) + 1];
        strcpy(_string, other._string);
    }

    return *this;
}

char& String::operator[] (int index)
{
    assert(index >= 0 && (size_t) index < strlen(_string));

    return _string[index];
}

char String::operator[] (int index) const
{
    assert(index >= 0 && (size_t) index < strlen(_string));

    return _string[index];
}

String String::operator+ (const String& other) const
{
    int size = strlen(_string);

    char* res = new char[strlen(other._string) + 1];
    strcpy(res, _string);
    strcpy(res + size, other._string);

    return String(res);
}

bool String::operator== (const String &other) const
{
    return !strcmp(_string, other._string);
}

bool String::operator!= (const String &other) const
{
    return !((*this) == other);
}




