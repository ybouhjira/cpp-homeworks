#include <gtest/gtest.h>
#include "../src/istring.h"

using namespace std;

TEST(StringTest, Size) {
    ILISI::String str("Hello");
    EXPECT_EQ(5, str.longueur());
}

TEST(StringTest, Concat) {
    ILISI::String str1("Hello"), str2(str1);

    EXPECT_EQ(ILISI::String("HelloHello"), str1 + str2);

}

TEST(StringTest, ArrayOperator) {
    const ILISI::String constString("Hello");

    EXPECT_EQ('H', constString[0]);
    EXPECT_EQ('o', constString[4]);

    ILISI::String str("cut");
    str[1] = 'a';

    EXPECT_EQ(ILISI::String("cat"), str);
}

TEST(StringTest, ComparOperator) {
    ILISI::String str1 = "C++";
    const ILISI::String str2 = "C++ ";

    EXPECT_TRUE(str1 != str2);
    EXPECT_TRUE(!(str1 == str2));

    str1 = str2;

    EXPECT_TRUE(str1 == str2);
    EXPECT_TRUE(!(str1 != str2));
}

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
